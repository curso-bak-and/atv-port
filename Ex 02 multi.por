programa
{
	
	funcao inicio()
	{
		inteiro valorN, trans 

		escreva("Digite o valor: ") 
		leia(valorN)

		
		
		se(valorN < 0){
			trans = valorN *-1
			escreva("O valor é: ", trans) 	
		}
		
		se(valorN > 0) {
			escreva("O valor é: ",valorN) 
			
		}
	}
}

/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 195; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */