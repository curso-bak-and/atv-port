programa {
	funcao inicio() {
		real imc, peso, altura  
		
		escreva("Digite seu peso: ")
		leia(peso)
		
	    escreva("Digite seu altura: ")
		leia(altura)
		
		imc = peso / (altura * altura)
		
		se(imc < 18.5){
		    escreva("Abaixo do peso.")
		}
		se(imc >= 18.5 e imc <25){
		    escreva("Peso normal.")
		}
		se(imc >= 25 e imc <30){
		   
		   escreva("Sobrepeso.") 
		}
		se(imc >=30 e imc <35){
		    escreva("Obesidade grau 1.")
		}   
	
	    se(imc >=35 e imc <40){
	        escreva("Obesidade grau 2.")
	    }
	
	    se(imc >=40){
	        escreva("Obesidade grau 3.")
	    }
	}
} 
