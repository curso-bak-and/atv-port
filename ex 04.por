programa
{
	
	funcao inicio()
	{
		inteiro num  

		escreva("Digite o valor: ")
		leia(num) 

		se(num%5 == 0){
			escreva("O número ", num, " é divisivel por 5.") 
		}
		se(num%7 == 0){
			escreva("\nO número ", num, " é divisivel por 7.") 
		}
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 190; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */