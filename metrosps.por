programa
{
	
	funcao inicio()
	{
		real distancia, tempo, metrosps

		escreva("Digite a distancia em quilometros: ")
		leia(distancia)

		escreva("Digite o tempo gasto para percorrer essa distancia:" )
		leia(tempo)

		metrosps = (distancia*1000)
		metrosps = tempo*60

		escreva("A distancia percorrida em metrosps foi: " , metrosps, "M")
		escreva("\n o tempo em segundos percorrido foi: " ,metrosps, "seg")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 421; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */