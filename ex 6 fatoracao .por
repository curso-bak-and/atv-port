programa
{
	
	funcao inicio()
	{
	inteiro num, fatoracao

		escreva("Digite o numero que será fatorado: ")
		leia(num)

		para(fatoracao = 1; num > 0 ; num--){	
			 fatoracao = num*fatoracao
			 
		     } escreva("Fatorial: " + fatoracao)	
				
	}
}

/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 252; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = {numero, 6, 9, 6};
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */