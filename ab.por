programa
{
	
	funcao inicio()
	{
		real A, B, troca

		escreva("valor A:")
		leia(A)

		escreva("valor B: ")
		leia(B)

		troca = A
		A = B
		B = troca

		escreva(" o nov0 valor da A é: " , A)
		escreva("\no novo valor de B é: " , B)
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 238; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */