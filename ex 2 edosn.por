programa
{
	
	funcao inicio()
	{		inteiro finish, start

			escreva("Digite o valor inicial da sua senha: ")
			leia(start)

			escreva("Digite o valor final da senha: ")
			leia(finish)

			para(start = start +1; start < finish;start++)
			escreva(start + " ")		
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 55; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */