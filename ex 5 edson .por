programa
{
	
	funcao inicio()
	{
	inteiro primeiro, contador, numDePar = 0
	
		escreva("Digite o primeiro valor: ")
		leia(primeiro)
		
		para(contador = 2; contador <= primeiro; contador++){
				se(contador %2==0 ){
					numDePar++				
				}
		}
		escreva("A quantidade de numeros pares no periodo entre 1 e " + primeiro + " é: " + numDePar)
	} 
}

/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 190; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */