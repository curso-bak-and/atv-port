programa
{
	
	funcao inicio()
	{
	    real horasTrabalhada, valorHora, percentualDesconto, salarioBruto, salarioLiquido, valorDesconto

	     escreva("Digite a quantidade de hosras trabalhadas: " )
	     leia(horasTrabalhada)

	     escreva("Digite o valor da hora: ")
	     leia(valorHora)

	     escreva("Digite o percentual de descontos %: ")
	     leia(percentualDesconto)

	     salarioBruto = horasTrabalhada *valorHora
	     valorDesconto = (percentualDesconto/100) * salarioBruto
	     salarioLiquido = salarioBruto - valorDesconto

	     escreva("Salrio Bruto: " ,salarioBruto)
	     escreva("\nSalario LIquido: " ,salarioLiquido)
	     
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 608; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */