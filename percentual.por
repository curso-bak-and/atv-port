programa
{
	
	funcao inicio()
	{
	     inteiro A,B ,C ,votosN ,votosB , eleitores , votosValidos
	     real  percentualVV,percentualVVA,percentualVVB,percentualVVC , percentualVN, percentualVB

          escreva("Quantidade de Votos Nulos: ")
          leia(votosN)

          escreva("Quantidade de votos em Branco: ")
          leia(votosB)

          escreva("Quantidade de votos que o candidado A teve: ")
          leia(A)

          escreva("Quantidade de votos que o candidado B teve: ")
          leia(B)

          escreva("Quantidade de votos que o candidado C teve: ")
          leia(C)

          
          votosValidos = A + B + C
	     eleitores = votosN + votosB +  votosValidos

          percentualVV = (votosValidos*100) / eleitores
          percentualVVA = (A*100) / eleitores
          percentualVVB = (B*100) / eleitores
          percentualVVC = (C*100) / eleitores
          percentualVN = (votosN * 100) / eleitores 
          percentualVB = (votosB * 100) / eleitores

          escreva("percentual de votos validos é: " + percentualVV + "%") 
          escreva("\npercentual de votos validos em A é: " + percentualVVA + "%")
          escreva("\npercentual de votos validos em B é: " + percentualVVB + "%")
          escreva("\npercentual de votos validos em C é: " + percentualVVC + "%")
          escreva("\npercentual de votos nulos é: " + percentualVN + "%" )
          escreva("\npercentual de votos brancos é: " + percentualVB + "%")

          // para não criar 300 variaveis, é possivel resumi-lás dessa maneira 
          //percentualVVA = (A*100) / eleitores
         // escreva("\npercentual de votos validos em A é: " + percentualVVA + "%") para --> escreva("Valor A: ", ((A * 100) / eleitores), "%")
  
                     
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 1748; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */